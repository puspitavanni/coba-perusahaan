import java.util.*;

public class PerusahaanInduk extends Perusahaan {

    private int noInduk;
    private List<PerusahaanInduk> listInduk = new ArrayList<>();

    public PerusahaanInduk(String nama, Alamat alamat, String telp, String fax, String email, int noInduk) {
        super(nama, alamat, telp, fax, email);
        this.noInduk = noInduk;
    }

    public int getNoInduk() {
        return noInduk;
    }

    public void addInduk(PerusahaanInduk i) {
        this.listInduk.add(i);
    }

    public String detailInduk() {
        String rekap = "";
        for (PerusahaanInduk induk : listInduk) {
            rekap += induk.getNama() + " - " + induk.getNoInduk();
        }
        return rekap;
    }

    public String toString() {
        return ("Perusahaan Induk " + getNama() + "\nNama: " + getNama() + "\n" + getAlamat() + "\nNo Telp: "
                + getTelp() + "\nFax: " + getFax() + "\nEmail: " + getEmail() + "\nNo Induk: " + getNoInduk()
                + "\nBentuk Usaha: " + bentukUsaha(1) + "\nStatus Milik: " + statusMilik(1) + "\nStatus Perusahaan: "
                + statusPerusahaan(1));
    }
}