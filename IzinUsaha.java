import java.util.*;

public class IzinUsaha {
    private int no;
    private String tanggal;
    private String oleh; // dikeluarkan oleh siapa

    IzinUsaha(int no, String tanggal, String oleh) {
        this.no = no;
        this.tanggal = tanggal;
        this.oleh = oleh;
    }

    public int getNo() {
        return this.no;
    }

    public String getTanggal() {
        return this.tanggal;
    }

    public String getOleh() {
        return this.oleh;
    }
}
